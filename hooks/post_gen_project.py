#!/usr/bin/env python
import httplib, json
from base64 import b64encode

def bb_request(verb, path, headers, data):
    conn = httplib.HTTPSConnection("api.bitbucket.org")

    conn.request(verb, path, data, headers)
    response = conn.getresponse()
    print response.status, response.reason
    conn.close()

def bb_put(user, password, path, data):
    auth_params = user + ":" + password
    auth = b64encode(auth_params).decode("ascii")

    headers = {"Content-Type": "application/json" , "Authorization": "Basic %s" % auth }
    bb_request("PUT", path, headers, data)

def bb_post(user, password, path, data):
    auth_params = user + ":" + password
    auth = b64encode(auth_params).decode("ascii")

    headers = {"Content-Type": "application/json" , "Authorization": "Basic %s" % auth }
    bb_request("POST", path, headers, data)

def create_repo_core(user,password,team, repo, data):
    path = "/2.0/repositories/" + team + "/" + repo
    json_data = json.dumps(data)

    bb_post(user, password, path, json_data)


def create_repo_pipeline(user, password, team, repo, data):
    path = "/2.0/repositories/" + team + "/" + repo + "/pipelines_config"
    json_data = json.dumps(data)

    bb_put(user, password, path, json_data)

def create_repo(user, password, team, repo, data):
    create_repo_core(user, password, team, repo, data["repo"])
    create_repo_pipeline(user, password, team, repo, data["pipelines"])


user = "{{cookiecutter.bitbucket_user}}"
password = "{{cookiecutter.bitbucket_password}}"

team = "{{cookiecutter.bitbucket_team}}"
repo = "{{cookiecutter.project_slug}}"

data = {    
    "repo": {
        "scm": "git", 
        "project": { 
            "key": "{{cookiecutter.bitbucket_project}}"
        },
        "description": "{{cookiecutter.description}}",
        "language": "{{cookiecutter.language}}",
        "is_private": "{{cookiecutter.is_private}}",
        "has_issues": "{{cookiecutter.has_issues}}",
        "has_wiki": "{{cookiecutter.has_wiki}}",
        "fork_policy": "{{cookiecutter.fork_policy}}",
    },
    "pipelines": {
        "enabled": "{{cookiecutter.enable_pipelines}}"
    }
}

create_repo(user, password, team, repo, data)
